package quants

import (
	"image"
	"image/color"
	"math"
	"math/rand"
	"time"
)

type point struct {
	r uint32
	g uint32
	b uint32
	a uint32
}

type kColor struct {
	point
	centroid *point
}

type kmeans struct {
	numColors int
	numCycles int
	centroids []point
	colors    []kColor
	image     image.Image
}

// genRandom generates random numbers 0 - 255
func genRandom(r *rand.Rand) uint32 {
	num := r.Intn(256)
	return uint32(num)
}

func createKmeans(colors int, cycles int, i image.Image) kmeans {
	return kmeans{colors, cycles, make([]point, colors), make([]kColor, colors), i}
}

func (k kmeans) populateColors() {

}

func (k kmeans) initCentroids() {
	r := rand.New(rand.NewSource(time.Now().UTC().UnixNano()))
	for _, p := range k.centroids {
		p.r = genRandom(r)
		p.g = genRandom(r)
		p.b = genRandom(r)
		p.a = genRandom(r)
	}
}

// assignColors takes colors from the image supplied and assigns the nearest centroid
// must be run after initCentroids
// modified from StackOverflow question #33186783 by user Kesarion
// https://stackoverflow.com/questions/33186783/get-a-pixel-array-from-from-golang-image-image
func (k kmeans) assignColors() [][]kColor {
	bounds := k.image.Bounds()
	width, height := bounds.Max.X, bounds.Max.Y
	var pixels [][]kColor
	for y := 0; y < height; y++ {
		var row []kColor
		for x := 0; x < width; x++ {
			r, g, b, a := k.image.At(x, y).RGBA()
			nearestDistance := math.MaxFloat64
			var nearestCentroid *point
			for _, c := range k.centroids {
				distance := math.Sqrt(math.Pow((float64)(r-c.r), 2) + math.Pow((float64)(g-c.g), 2) + math.Pow((float64)(b-c.b), 2) + math.Pow((float64)(a-c.a), 2))
				if distance < nearestDistance {
					nearestCentroid = &c
					nearestDistance = distance
				}
			}
			row = append(row, kColor{point{r, g, b, a}, nearestCentroid})
		}
		pixels = append(pixels, row)
	}
	return pixels
}

func (k kmeans) recalculateCentroids() {

}

func (k kmeans) run(m image.Image) color.Palette {
	k.initCentroids()
}

func (k kmeans) Quantizer(p color.Palette, m image.Image) color.Palette {

}
